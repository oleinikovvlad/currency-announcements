<?php

/**
 * @param string $contact-mail
 * @param string $text-mail
 * @param string $key-coinmarketcap
 * @param string $key-openexchangerates
 * @param string $key-localbitcoins
 * @param string $secret-localbitcoins
 * @param string $ads_show
 * @param int    $use_localbitcoins 
 */
?>

<div class="wrap">
<h2>Currency announcements</h2>
<form method="post" action="">
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Contact email</th>
            <td><input type="text" name="contact-mail" value="<?= $contact_mail?>" /></td>
            <td>
                <a href="<?= "?page=ca-main-setting&action=addEmails"?>"> Дополнительная почта</a>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Email text</th>
            <td><input type="text" name="text-mail" value="<?=$text_mail?>" /></td>
        </tr>
        <tr valign="top">
            <th scope="row">Key coinmarketcap</th>
            <td><input type="text" name="key-coinmarketcap" value="<?= $key_coinmarketcap?>" /></td>
        </tr>
         
        <tr valign="top">
            <th scope="row">Key openexchangerates</th>
            <td><input type="text" name="key-openexchangerates" value="<?= $key_openexchangerates?>" /></td>
        </tr>
        <tr valign="top">
            <th scope="row">Key LocalBitcoins</th>
            <td><input type="text" name="key-localbitcoins" value="<?= $key_localbitcoins?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">Secret LocalBitcoins</th>
            <td><input type="text" name="secret-localbitcoins" value="<?= $secret_localbitcoins?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Use localbitcoins?</th>
        <td><input type="checkbox" name="use-localbitcoins" value="true" <?php if ($use_localbitcoins=='1') echo 'checked'; ?>/></td>
        </tr>

        <tr valign="top">
        <th scope="row">What ads are showing?</th>
        <td>
            <select name="ads_show">
                <option <?php if ($ads_show == 'all') echo 'selected';?> value="all">All</option>
                <option <?php if ($ads_show == 'sell') echo 'selected';?> value="sell">SELL</option>
                <option <?php if ($ads_show == 'buy') echo 'selected';?> value="buy">BUY</option>
            </select>
        </td>
        </tr>
    </table>
    
    <p class="submit">
    <input type="submit" name="update-main-info" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>

</form>
</div>

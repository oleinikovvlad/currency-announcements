<?php
/**
 * @param caCommon\models\TableView[] $tables
 * @param caCommon\models\Settings  $setting
 */
?>

<div class="wrap">
<h2>Add Email Addresses</h2>
<form method="post" action="">
    <table class="form-table">
        <?php 
            foreach ($tables as $table) {
                if (isset($setting->setting_value[$table->id_key])) {
                    $value = $setting->setting_value[$table->id_key];
                } else {
                    $value = '';
                }
                ?>
                    <tr>
                        <td>
                            <?= $table->id_name ?>
                        </td>
                        <td>
                            <input type="string" name="<?= $table->id_key ?>" value ="<?= $value ?>"/>
                        </td>
                    <td>
                <?php
            }
        ?>
    </table>
    
    <p class="submit">
    <input type="submit" name="update" class="button-primary" value="<?php _e('update') ?>" />
    </p>

</form>
</div>

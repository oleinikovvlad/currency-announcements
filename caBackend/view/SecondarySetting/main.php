<?php

/**
 * @var caCommon\models\AnnouncementsInfo[] $announcementsInfo
 * 
 * @var string $curentTab текущий срез оферов
 * @var caCommon\models\TableView[]  $tabViews доступные срезы
 */

include dirname(__FILE__).'/../../../ca-core-request.php';
include dirname(__FILE__).'/../../../ca-admin/ca-currency-info.php'; 
created_table_currency($currency_info);

$quantity= count($announcementsInfo);
$offer = null;
$bgColor = '';

?>
<table><tr>
    <?php foreach($tabViews as $tabElement): ?>
        <td>
            <?php if ($tabElement->id_key === $curentTab): ?>
                <span>
                    <?php if ($tabElement->id_key !== 'main'): ?>
                        <a href="<?= "?page=ca-secondary-setting&action=createTabView&id={$tabElement->id}"?>">&#9998;</a>
                    <?php endif; ?>
                    <?= $tabElement->id_name; ?></span>
            <?php else: ?>
                <a href="<?= "?page=ca-secondary-setting&tableView={$tabElement->id_key}"?>"><?= $tabElement->id_name; ?></a>
            <?php endif; ?>

        </td>
    <?php endforeach;?>
    <td>
        <a href="<?= "?page=ca-secondary-setting&action=createTabView"?>"> + </a>
    </td>
</tr></table>

<div class="wrap" onload="new_order()">
    <h2>Setting announcement</h2>
    <form method="post" action="">
        <table class="form-table" border="1">
            <?php foreach ($announcementsInfo as $offer): ?>
                <?php $id             = $offer->id; ?>
                <?php $visibility     = $offer->visibility; ?>
                <?php $activity       = $offer->activity; ?>
                <?php $currencyName   = $offer->currency_name; ?>
                <?php $payment        = $offer->payment; ?>
                <?php $extraCharge    = $offer->extra_charge; ?>
                <?php $restrictionMin = $offer->restriction_min; ?>
                <?php $restrictionMax = $offer->restriction_max; ?>
                <?php $currencyValue  = $offer->currency_value; ?>
                <?php $bgColor = $bgColor ? '' : '#b1b1b1' ?>
                <tr valign="top" style="<?="background-color:{$bgColor}"?>">
                    <td rowspan="2" name="delet-announcements" id="<?="delet-announcements-{$id}"?>" width="30px" >
                        <button name="delet" class="button-link-delete" id="<?="delet-announcements-{$id}"?>" value="<?= $id?>">X</button>
                        <input name="<?= "delet-{$id}"?>" value="<?= $id?>" hidden/>
                    </td>
                    <td width="180px">
                        <select name="<?="visibility-{$id}"?>" id="<?="visibility-{$id}"?>">
                        <?php if ($visibility === "0"): ?>
                            <option value="0">Скрыть объявление</option>
                            <option value="1">Показать объявление</option>
                        <?php elseif ($visibility === "1"): ?>
                            <option value="1">Показать объявление</option>
                            <option value="0">Скрыть объявление</option>
                        <?php else: ?>
                            <option value="none" hidden="">Видимость объявления</option>
                            <option value="0">Скрыть объявление</option>
                            <option value="1">Показать объявление</option>
                        <?php endif; ?>
                        </select>
                    </td>
                    <td width="100px">
                        <select name="<?="activity-{$id}"?>" id="<?="activity-{$id}"?>">
                        <?php if ($activity === "0"): ?>
                            <option value="0">Купить</option>
                            <option value="1">Продать</option>
                        <?php elseif ($activity === "1"): ?>
                            <option value="1">Продать</option>
                            <option value="0">Купить</option>
                        <?php else: ?>
                            <option value="none" hidden="">Выберите тип</option>
                            <option value="0">Купить</option>
                            <option value="1">Продать</option>
                        <?php endif; ?>
                        </select>
                    </td>
                    <td width="70px">
                        <select name="<?="currency-name-{$id}"?>" id="<?="currency-name-{$id}"?>">
                        <?php if(in_array($currencyName, $availableСurrency)): ?>
                            <option value="<?=$currencyName?>"><?=$currencyName?></option>
                        <?php else: ?>
                            <option value="none" hidden="">Выберите валюту</option>
                        <?php endif; ?>
                    <?php foreach ($availableСurrency as $currencyKey): ?>
                        <?php if ($currencyKey !== $currencyName): ?>
                            <option value="<?=$currencyKey?>"><?=$currencyKey?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                        </select>
                    </td>
                    <td width="100px">
                        <select name="<?="payment-{$id}"?>" id="<?="payment-{$id}"?>">
                        <?php if ($payment === 'cash-deposit'): ?>
                            <option value="cash-deposit">Cash deposit</option>
                            <option value="bank-transfer">Bank transfer</option>
                        <?php elseif ($payment === 'bank-transfer'): ?>
                            <option value="bank-transfer">Bank transfer</option>
                            <option value="cash-deposit">Cash deposit</option>
                        <?php else: ?>
                            <option value="none" hidden="">Оплата</option>
                            <option value="cash-deposit">Cash deposit</option>
                            <option value="bank-transfer">Bank transfer</option>
                        <?php endif; ?>
                        </select>
                    </td>
                    <td>Процент к цене <input type="number" step="0.01" name="<?= "extra-charge-{$id}"?>" id="<?= "extra-charge-{$id}"?>" value="<?= $extraCharge ?>" /></td>
                </tr>
                <tr valign="top"  style="<?="background-color:{$bgColor}"?>">
                    <td colspan="4"> 
                        Лимиты на операцию
                        <input type="number" id="<?="restriction-min-{$id}"?>" name="<?="restriction-min-{$id}"?>" value="<?= $restrictionMin ?>"/>
                        <input type="number" id="<?="restriction-max-{$id}"?>" name="<?="restriction-max-{$id}"?>" value="<?= $restrictionMax ?>"/>
                    </td>
                    <td> 
                        Коментарий
                        <textarea name="<?="currency-value-{$id}"?>" id="<?="currency-value-{$id}"?>"><?= $currencyValue ?></textarea>
                        <a href="<?= "?page=ca-secondary-setting&action=AddTranslite&translation={$id}"?>"> Добавить перевод</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr id="new-order">   
                <td colspan="6">
                    <a href="<?= "?page=ca-secondary-setting&action=createOffers&tableView={$curentTab}"?>" class="button-secondary" id="new-order-href">New order</a>
                </td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" name="save-announcements" class="button-primary" value="Save Changes" />
        </p>
    </form>
</div>

<?php
/**
* <script type="text/javascript" src="<?=plugins_url("currency-announcements/js/create-announcement.js")?>" defer></script>
*/
?>

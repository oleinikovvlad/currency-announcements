<?php
/**
 * @var caCommon\models\AnnouncementsInfo $model
 * @var array $availableСurrency
 * @var string $tableView
 */
?>

<div class="wrap">
    <h2>Создать новое предложение</h2>
    <form method="post" action="">
        <table class="form-table">
            <tr>
                <td>Видимость объявления:</td>
                <td>
                    <select name="visibility">
                    <?php if ($model->visibility === "0"): ?>
                        <option value="0">Скрыть объявление</option>
                        <option value="1">Показать объявление</option>
                    <?php else: ?>
                        <option value="1">Показать объявление</option>
                        <option value="0">Скрыть объявление</option>
                    <?php endif; ?>
                    </select>
                </td>
            </tr>
            <tr> 
                <td>Выберите тип</td>
                <td>
                    <select name="activity">
                    <?php if ($model->activity === "0"): ?>
                        <option value="0">Купить</option>
                        <option value="1">Продать</option>
                    <?php else: ?>
                        <option value="1">Продать</option>
                        <option value="0">Купить</option>
                    <?php endif; ?>
                    </select>
                </td>
            </tr>
            <tr> 
                <td>Выберите валюту:</td>
                <td>
                    <select name="currency_name">
                    <?php if(in_array($model->currency_name, $availableСurrency)): ?>
                        <option value="<?= $model->currency_name ?>"><?= $model->currency_name ?></option>
                    <?php endif; ?>
                    <?php foreach ($availableСurrency as $currencyKey): ?>
                        <?php if ($currencyKey !== $model->currency_name): ?>
                            <option value="<?=$currencyKey?>"><?=$currencyKey?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr> 
                <td>Оплата:</td>
                <td>
                    <select name="payment">
                    <?php if ($model->payment === 'cash-deposit'): ?>
                        <option value="cash-deposit">Cash deposit</option>
                        <option value="bank-transfer">Bank transfer</option>
                    <?php else: ?>
                        <option value="bank-transfer">Bank transfer</option>
                        <option value="cash-deposit">Cash deposit</option>
                    <?php endif; ?>
                    </select>
                </td>
            </tr>
            <tr> 
                <td>Процент к цене:</td>
                <td>
                    <input type="number" step="0.01" name="extra_charge" value="<?= $model->extra_charge ?>" />
                </td>
            </tr>
            <tr> 
                <td>Лимиты на операцию:</td>
                <td>
                    <input type="number" name="restriction_min" value="<?= $model->restriction_min ?>"/>
                    <input type="number" name="restriction_max" value="<?= $model->restriction_max ?>"/>
                </td>
            </tr>
            <tr> 
                <td>Коментарий:</td>
                <td>
                    <textarea name="currency_value"><?= $model->currency_value ?></textarea>
                </td>
            </tr>
            <tr colsapn="2">       
                <td>
                    <input type="hidden" name="table_view" value="<?= $tableView ?>"/>
                    <p class="submit">
                        <input type="submit" name="save" class="button-primary" value="Сохранить" />
                    </p>
                    <p class="submit">
                        <input type="submit" name="save-with-comment" class="button-primary" value="Сохранить и добавить перевод коментерия" />
                    </p>
                </td>
            </tr>
        </table>
    </form>
<div>
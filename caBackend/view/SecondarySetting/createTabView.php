<?php
/**
 * @var caCommon\models\TableView $model
 * @var array $availableСurrency
 */
?>

<div class="wrap">
    <h2>Создать новую таблицу с предложениями</h2>
    <form method="post" action="">
        <table class="form-table">
            <tr>
                <td>Название таблицы:</td>
                <td>
                    <input size="26" type="text" name="id_name" maxlength="32" value="<?=$model->id_name?>">
                </td>
            </tr>
            <tr> 
                <td>Ключ индетификатор</td>
                <td>
                    <input size="10" type="text" name="id_key" maxlength="5" value="<?=$model->id_key?>">
                </td>
            </tr>
            <tr>       
                <td>
                    <input type="hidden" name="id" value="<?= $model->id ?>"/>
                    <p class="submit">
                        <input type="submit" name="save" class="button-primary" value="Сохранить" />
                    </p>
                </td>
                <td>
                    <p class="submit">
                        <input type="submit" name="delete" class="button-primary" value="&#128465; Удалить" />
                    </p>
                </td>
            </tr>
            <tr colspan="2">
                <p>
                    <a href="?page=ca-secondary-setting">Вернуться назад</a>
                </p>
            </tr>
        </table>
    </form>
<div>
<?php

namespace caBackend\controllers;

class Controller
{
    protected $defaultAction = 'index';
    protected $action = '';

    public function beforeAction()
    {
        $this->action = $_GET['action'] ?? $this->defaultAction;

        $function = "action{$this->action}";
        if (!method_exists($this, $function)) {
            $function = $this->defaultAction;
        }

        $params = $this->bindActionParams($function, $_GET);

        call_user_func_array([$this, $function], $params);
    }

    public function bindActionParams($action, $params)
    {
        $method = new \ReflectionMethod($this, $action);

        $args = [];
        foreach ($method->getParameters() as $param) {
            $name = $param->getName();
            if (array_key_exists($name, $params)) {
                if (
                    PHP_VERSION_ID >= 70000
                    && ($type = $param->getType()) !== null
                    && $type->isBuiltin()
                    && ($params[$name] !== null || !$type->allowsNull())
                ) {
                    $typeName = PHP_VERSION_ID >= 70100 ? $type->getName() : (string)$type;

                    if ($params[$name] === '' && $type->allowsNull()) {
                        if ($typeName !== 'string') { // for old string behavior compatibility
                            $params[$name] = null;
                        }
                    } else {
                        switch ($typeName) {
                            case 'int':
                                $params[$name] = filter_var($params[$name], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);
                                break;
                            case 'float':
                                $params[$name] = filter_var($params[$name], FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);
                                break;
                            case 'bool':
                                $params[$name] = filter_var($params[$name], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                                break;
                        }
                    }
                }
                $args[] = $params[$name];
                unset($params[$name]);
            } elseif ($param->isDefaultValueAvailable()) {
                $args[] = $param->getDefaultValue();
            } 
        }

        return $args;
    }

    protected function render(string $view, ?array $params = [])
    {
        $filePath = $this->getFilePath($view);

        if (is_file($filePath)) {
            foreach ($params as $paramName => $value) {
                $$paramName = $value;
            }

            include $filePath;
        } // else {
        //     404
        // }
    }

    protected function redirect(array $params)
    {
        $url = home_url(). "/wp-admin/admin.php";
        $url = add_query_arg($params, $url);

        $this->render('redirect', ['url' => $url]);
    }

    private function getFilePath(string $view)
    {
        $className = get_called_class();
        $map = explode("\\", $className);
        if (count($map) === 3) {
            $map[1] = 'view';
            $map[2] = preg_replace(['/^Ca/', '/Controller/'], '', $map[2]);
        }
        $map[] = $view. '.php';

        return CA_DIR . implode('/', $map);
    }
}
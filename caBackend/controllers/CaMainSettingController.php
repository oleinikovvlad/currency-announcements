<?php

namespace caBackend\controllers;

use caCommon\models\Settings;
use caCommon\models\TableView;

class CaMainSettingController extends Controller
{
    public function index()
    {
        if(!empty($_POST['update-main-info'])){
            global $wpdb;
            $contact_mail           = $_POST['contact-mail'];
            $text_email             = $_POST['text-mail'];
            $key_coinmarketcap      = $_POST['key-coinmarketcap'];
            $key_openexchangerates  = $_POST['key-openexchangerates'];
            $key_localbitcoins      = $_POST['key-localbitcoins'];
            $secret_localbitcoins   = $_POST['secret-localbitcoins'];
            $ads_show               = $_POST['ads_show'];
            if(!empty($_POST['use-localbitcoins'])){
                $use_localbitcoins = 1;
            } else {
                $use_localbitcoins = 0;
            }
            $sql_request = "
            UPDATE ca_main_info 
            SET 
                contact_mail = '$contact_mail',
                text_mail = '$text_email',
                key_coinmarketcap = '$key_coinmarketcap',
                key_openexchangerates = '$key_openexchangerates',
                key_localbitcoins = '$key_localbitcoins',
                secret_localbitcoins = '$secret_localbitcoins',
                use_localbitcoins = b'$use_localbitcoins',
                ads_show = '$ads_show'
            WHERE ca_main_info.id = 1;
            ";
            $wpdb->query($sql_request);
        }
    
        
        $this->render('main', $this->getMainInfo());
    }

    public function actionAddEmails()
    {
        $settingName = 'table_email';
        $setting = Settings::findOne(['setting_name' => $settingName]);

        if ($_POST && $_POST['update']) {
            $data = $_POST;
            unset($data['update']);

            $setting->setting_name = $settingName;
            $setting->setting_value = json_encode($data);
            $setting->save();
        }

        if ($setting->setting_value) {
            $setting->setting_value = json_decode($setting->setting_value, true);
        } else {
            $setting->setting_value = [];
        }

        $this->render('addEmails', [
            'tables' => TableView::findAll(),
            'setting' => $setting,
        ]);
    }

    private function getMainInfo(): array
    {
        global $wpdb;
        $main_info = $wpdb->get_row( "SELECT * FROM ca_main_info", ARRAY_A);
        return  $main_info;
    }
}

<?php

namespace caBackend\controllers;

use caCommon\models\AnnouncementsComment;
use caCommon\models\AnnouncementsInfo;
use caCommon\models\AvailableСurrency;
use caCommon\models\Language;
use caCommon\models\TableView;

class CaSecondarySettingController extends Controller
{
    public function index()
    {
        if (!empty($_POST['delet'])){
            $this->deletAnnouncement($_POST['delet']);
        }
 
        $tableViewModel = new TableView();
        $tableViewModel->id_name = 'Основные';
        $tableViewModel->id_key = 'main';

        $tabViews = array_merge([$tableViewModel], TableView::findAll());

        $tableView = 'main';
        if (isset($_GET['tableView'])) {
            $tableView = $_GET['tableView'];
        }

        $announcementsInfo = AnnouncementsInfo::findAll(['table_view' => $tableView]);
        $dumpPost = $_POST;
        if (!empty($_POST['save-announcements'])) {
            foreach ($announcementsInfo as &$model) {
                $id = $model->id;
                $model->visibility      = $dumpPost["visibility-{$id}"];
                $model->activity        = $dumpPost["activity-{$id}"];
                $model->currency_name   = $dumpPost["currency-name-{$id}"];
                $model->payment         = $dumpPost["payment-{$id}"];
                $model->extra_charge    = $dumpPost["extra-charge-{$id}"];
                $model->restriction_min = $dumpPost["restriction-min-{$id}"];
                $model->restriction_max = $dumpPost["restriction-max-{$id}"];
                $model->currency_value  = $dumpPost["currency-value-{$id}"];
                $model->save();
            }
        }
    
        $this->render('main', [
            'announcementsInfo' => $announcementsInfo,
            'availableСurrency' => AvailableСurrency::VALUE,
            'curentTab' => $tableView,
            'tabViews' => $tabViews,
        ]);
    }

    public function actionCreateOffers(string $tableView = 'main')
    {
        $model = new AnnouncementsInfo();

        if (
            isset($_POST['save'])
            || isset($_POST['save-with-comment'])
        ) {
            $model->loadData($_POST);
            $model->save();
            
            if (isset($_POST['save-with-comment'])) {
                $this->redirect([
                    'page' => 'ca-secondary-setting',
                    'action' => 'AddTranslite',
                    'translation' => $model->id,
                    'tableView' => $tableView
                ]);
            } else {
                $this->redirect(['page' => 'ca-secondary-setting', 'tableView' => $tableView]);
            }
        }

        $this->render(
            'createOffer',
            [
                'model' => $model,
                'availableСurrency' => AvailableСurrency::VALUE,
                'tableView' => $tableView
            ]
        );
    }

    public function actionCreateTabView(int $id = 0)
    {
        $model = TableView::findOne(['id' => $id]);

        if (isset($_POST['save'])) {
            $model->loadData($_POST);
            $model->save();
        } elseif (isset($_POST['delete'])) {
            $this->deleteTableView($model);
            $this->redirect(['page' => 'ca-secondary-setting']);
        }

        $this->render('createTabView', ['model' => $model]);
    }

    public function actionAddTranslite(string $tableView = 'main')
    {
        global $wpdb;

        $languageMessage = '';
        $arrayTranslite = [];

        $translation = $_GET['translation'];
        $announcements = AnnouncementsInfo::findOne(['id' => $translation]);    
        if ($announcements) {
            $arrayTranslite = AnnouncementsComment::findAll(['id_announc' => $translation]);
            $languages = Language::findAll();
            foreach ($languages as $lang) {
                $sortLang[$lang->id] = $lang;
            }
            
            $languageId = $_POST['language-id'] ?? '';
            $currencyValue = $_POST['currency-value'] ?? '';
            
            if (isset($_POST['add-announcements-comment']) && $languageId == 'none') {
                $languageMessage = 'Нужно выбрать индетификатор перевода';
            } elseif (isset($_POST['add-announcements-comment'])) {
                $model = new AnnouncementsComment();
                $model->id_announc = (int) $translation;
                $model->id_langth = (int) $languageId;
                $model->comment = $currencyValue;
                $model->save();

                $currencyValue = '';
                $arrayTranslite[] = $model;
            } elseif ($_POST) {    
                foreach ($arrayTranslite as $index => $translite) {
                    $postName = "delete-announcements-comment-".$translite->id;
            
                    if(!empty($_POST[$postName])) {
                        $wpdb->delete(
                            'ca_announcements_comment', 
                            array( 'id'=> $translite->id), 
                            array( '%d' )
                        );
                        
                        unset($arrayTranslite[$index]);
                        break;
                    }     
                }
            }
        }

        $this->render('addTranslite', [
            'announcements' => $announcements,
            'langModels' => $languages,
            'languageMessage' => $languageMessage,
            'arrayTranslite' => $arrayTranslite,
            'sortLang' => $sortLang,
            'currencyValue' => $currencyValue,
            'tableView' => $tableView,
        ]);
    }

    private function deletAnnouncement($id_announc){
        global $wpdb;
        $wpdb->delete(
            'ca_announcements_info',
            ['id'=> $id_announc], 
            ['%d']
        );
        $wpdb->delete(
            'ca_announcements_comment',
            ['id_announc'=> $id_announc],
            ['%d']
        );
    }

    private function deleteTableView(TableView $model)
    {
        $announcements = AnnouncementsInfo::findAll(['table_view' => $model->id_key]);

        foreach ($announcements as $offer) {
            $this->deletAnnouncement($offer->id);
        }

        global $wpdb;
        $wpdb->delete(
            TableView::tableName(),
            ['id'=> $model->id], 
            ['%d']
        );
    }
}
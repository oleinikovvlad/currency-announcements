<?php

use caBackend\controllers\CaSecondarySettingController;

class App
{
    public function __construct()
    {
        // add_action('admin_menu', [$this, 'createMenu']);
    }

    public function createMenu()
    {
        // add_menu_page('CA Plugin Settings', 'CA Plugin Settings', 'manage_options', 'ca-main-setting');
        // add_submenu_page( 'ca-main-setting', 'CA Setting Main', 'Main', 'manage_options', 'ca-main-setting', [$this, 'test']);
        add_submenu_page('ca-main-setting', 'CA Setting Secondary', 'Secondary', 'manage_options', 'ca-secondary-setting', [new CaSecondarySettingController(), 'beforeAction']);
        // add_submenu_page( 'ca-main-setting', 'CA Basic Settings', 'Basic Settings', 'manage_options', 'ca-basic-settings', [$this, 'test']);
        // add_submenu_page( 'ca-main-setting', 'CA Core Tranlite', 'Core Tranlite', 'manage_options', 'ca-core-translite', [$this, 'test']);
    }
}
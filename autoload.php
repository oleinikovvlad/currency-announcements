<?php

spl_autoload_register(function ($inputName) {
    $className = explode('\\', $inputName);
    $className = end($className);

    $classFile = str_replace('\\', '/', $inputName) . '.php';
    $classFile = CA_DIR . $classFile;

    if (!is_file($classFile)) {
        return $className;
    }

    include $classFile;
});
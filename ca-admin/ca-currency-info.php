<?php
function created_table_currency($currency_info)
{
    $table = '<table border="1" >';
    $size_array = count($currency_info);
    if ($size_array > 1){
        
        global $value_BTC; 
        $value_BTC =    $currency_info[0]->currency_value;
        $date      =    $currency_info[0]->updated_date;

        $table    .=    created_row_date($date);
        $table    .=    created_row_fiat();
        $index     =    1;

        $date      =   $currency_info[1]->updated_date;
        $table    .=   created_row_date($date);

        while ($index < $size_array){
            
            $name  =    $currency_info[$index]->currency_name;
            $value =    $currency_info[$index]->currency_value;

            $index++;
            $table .= created_row_currency($name, $value);
        }
    }  
    $table .= '</tr></table>';
    echo $table;
} 

function created_row_date($date){
    $date_now = strtotime(date('c', time()));
    $date = strtotime($date);
    $difference_t =  ($date_now - $date)/60;

    $table = '<tr><td colspan="10">Обновленно : <b>'.intval($difference_t).' мин.</b> назад</td></tr><tr>';

    return $table;
}

function created_row_fiat(){
    global $value_BTC;

    $table = '<td>Курс BTC/USD = '.$value_BTC.'</td></tr>';

    return $table;
}

function created_row_currency($name, $value){
    global $value_BTC;
    $value = $value_BTC * $value;
    
    $table = '<td>Курс BTC/'.$name.' = '.$value.'</td>';
    return $table;
}

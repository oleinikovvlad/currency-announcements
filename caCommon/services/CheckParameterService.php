<?php

namespace caCommon\services;

use caCommon\models\Language;
use caCommon\models\TableView;

/**
* 
**/
class CheckParameterService
{
    public function languageParameter($param): ?string
    {
        $response = null;
        if (!is_null($param)) {
            $languages = Language::findAll();
            foreach ($languages as $lang) {
                if ($lang->lang === $param) {
                    $response = $lang->lang;
                    break;
                }
            }
        }

        return $response;
    }

    public function tableNameParameter($param): string
    {
        $response = 'main';
        if ($response !== $param && !is_null($param)) {
            $tables = TableView::findAll();
            foreach ($tables as $table) {
                if ($table->id_key === $param) {
                    $response = $table->id_key;
                    break;
                }
            }
        }

        return $response;
    }
}

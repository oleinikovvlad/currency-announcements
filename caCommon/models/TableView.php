<?php

namespace caCommon\models;

class TableView extends Model
{
    protected $tableName = "ca_table_view";
    protected $tablePrefix = 'catv';
    protected $typeField = ['%s', '%s'];

    protected $attribute = [
        'id' => '0',
        'id_name' => '',
        'id_key' => ''
    ];

    public static function tableName()
    {
        return 'ca_table_view';
    }
}
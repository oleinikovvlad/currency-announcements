<?php

namespace caCommon\models;

class PluginTranslite extends Model
{
    protected $tableName = "ca_plugin_translite";
    protected $tablePrefix= 'capt';
    protected $typeField = ['%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'];

    protected $attribute = [
        'id' => '0',
        'translite_id' => 'default',
        'column_sell' => 'Sell bitcoins',
        'button_sell' => 'sell',
        'column_buy' => 'Buy bitcoins',
        'button_buy' => 'buy',
        'text_price' => 'Price:',
        'text_trade_limits' => 'Trade limits:',
        'text_type_payment' => 'Type of payment:',
        'bank_transfer' => 'Bank transfer',
        'cash_deposit' => 'Cash deposit',
        'text_comment' => 'Comment:',
        'text_sell_question' => 'How much do you want to sell bitcoins?',
        'text_buy_question' => 'How much do you want to buy bitcoins?',
        'text_user_comment' => 'Leave your comment',
        'text_traide_limits' => 'Trade limits: bigger {min} and less {max}',
        'text_error_limit' => 'Please enter your value inside the trade limits',
        'text_error_critical' => 'Sorry..We cannot process this request',
        'button_send' => 'SEND',
        'button_cancel' => 'CANCEL',
        'text_success_sell' => 'You requested to sell:',
        'text_success_buy' => 'You requested to buy:',
        'text_error_sell' => 'Something run in incorrect way, please reload the page or wait for 1 minute. Thank you for your patience!',
        'text_error_buy' => 'Something run in incorrect way, please reload the page or wait for 1 minute. Thank you for your patience!',
        'text_instruction' => 'Please click the Chat button for receiving further instructions from us or send a message through Telegram: https://t.me/onlybestoffers.'.PHP_EOL.'If you want to apply for a new trade, please reload the page.',
    ];

    public static function tableName()
    {
        return 'ca_plugin_translite';
    }
}

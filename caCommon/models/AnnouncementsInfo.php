<?php

namespace caCommon\models;

class AnnouncementsInfo extends Model
{
    protected $tableName = "ca_announcements_info";
    protected $tablePrefix = 'caai';
    protected $typeField = ['%d', '%d', '%s', '%s', '%f', '%d', '%d', '%s', '%s'];

    protected $attribute = [
        'id' => '0',
        'visibility' => '0',
        'activity' => '0',
        'currency_name' => '',
        'payment' => '',
        'extra_charge' => '',
        'restriction_min' => '',
        'restriction_max' => '',
        'currency_value' => '',
        'table_view' => 'main',
    ];

    public static function tableName()
    {
        return 'ca_announcements_info';
    }
}
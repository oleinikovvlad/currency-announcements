<?php

namespace caCommon\models;

class Settings extends Model
{
    protected $tableName = "ca_settings";
    protected $tablePrefix = 'caseting';
    protected $typeField = ['%s', '%s'];

    protected $attribute = [
        'id' => '0',
        'setting_name' => '',
        'setting_value' => ''
    ];

    public static function tableName()
    {
        return 'ca_settings';
    }
}

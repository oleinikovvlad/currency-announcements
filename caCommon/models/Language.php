<?php

namespace caCommon\models;

class Language extends Model
{
    protected $tableName = "ca_language";
    protected $tablePrefix = 'cal';
    protected $typeField = ['%s'];

    protected $attribute = [
        'id' => '0',
        'lang' => '',
    ];

    public static function tableName()
    {
        return 'ca_language';
    }
}
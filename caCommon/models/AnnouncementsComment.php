<?php

namespace caCommon\models;

class AnnouncementsComment extends Model
{
    protected $tableName = "ca_announcements_comment";
    protected $tablePrefix = 'caac';
    protected $typeField = ['%d', '%d', '%s'];

    protected $attribute = [
        'id' => '0',
        'id_announc' => 0,
        'id_langth' => 0,
        'comment' => '',
    ];

    public static function tableName()
    {
        return 'ca_announcements_comment';
    }
}
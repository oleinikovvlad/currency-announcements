<?php

namespace caCommon\models;

class Model
{
    protected $db;

    protected $tablePrefix = '';

    protected $typeField = [];

    protected $sql = [];
    
    protected $request = '';

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb; 
    }

    public function __get($value)
    {
        return $this->attribute[$value] ?? null;
    }

    public function __set($key, $value)
    {
        if (isset($this->attribute[$key])) {
            $this->attribute[$key] = $value;
        }
    }

    public static function className()
    {
        return get_called_class();
    }

    public static function findAll(?array $param = [])
    {
        $models = [];
        $query = new static; 
        $result = $query->where($param)->createCommand()->all();
        
        foreach ($result as $item) {
            $model = new static;
            $models[] = $model->loadData($item);
        }

        return $models;
    }

    /**
     * Найдет одно единсвенное совпадение по переденным параметрам
     */
    public static function findOne(array $param)
    {
        $model = new static; 
        return $model->where($param)->createCommand()->one();
    }

    /**
     * Объявление структуры запроса SELECT 
     */
    public function select($value = false)
    {
        $request = '';
        if (is_string($value)) {
            $request = $value; 
        } elseif (is_array($value)) {
            foreach ($value as $param) {
                $request .= $request
                    ? ', '.$param
                    : $param;
            }
        } else {
            $request = '*';
        }

        $this->sql['SELECT'] = $request;

        return $this;
    }

    /**
     * Объявление структуры запроса WHERE 
     */
    public function where($value = false)
    {
        if (is_array($value)) {
            $bdPrefix = $this->tablePrefix;
            foreach ($value as $row => $param) {
                $condition = $bdPrefix.".".$row." = ";
                $condition .= is_numeric($param) ? $param : "'{$param}'";
                $this->sql['WHERE'][] = $condition;
            }
        }

        return $this;
    }


    public function createCommand(): Model
    {
        $this->select();
        
        $request = "SELECT ".$this->sql['SELECT'];
        $request .= " FROM " . $this->tableName . " as ". $this->tablePrefix;

        if (isset($this->sql["WHERE"])) {
            $request .= " WHERE ";
            $useAnd = false;
            foreach ($this->sql["WHERE"] as $condition) {
                $request .= ($useAnd ? " AND " : "") . $condition;
                $useAnd = true;
            }
        }

        $this->request = $request;

        return $this;
    }

    private function one()
    {
        $response = [];
        if ($this->request) {
            $response = $this->db->get_row($this->request, ARRAY_A) ?? [];
        }

        $this->constructModel($response);
        return $this;
    }

    private function all(): array
    {
        $response = [];
        if ($this->request) {
            $response = $this->db->get_results($this->request);
        }

        return $response;
    }

    private function constructModel($data)
    {
        if (!empty($data)) {
            $this->loadData($data);
        }
    }

    public function loadData($data): Model
    {
        foreach ($data as $key => $value) {
            if (($this->$key !== null)) {
                $this->$key = $value;
            }
        }

        return $this;
    }

    public function save(): Model
    {
        if ($this->id === '0') {
            $this->createModel();
        } else {
            $this->updateModel();
        }

        return $this;
    }

    private function createModel()
    {
        $params = $this->attribute;
        unset($params['id']);

        $data = $this->db->insert(
            $this->tableName,
            $params,
            $this->typeField
        );

        $this->id = $this->db->insert_id;
    }

    private function updateModel()
    {
        $params = $this->attribute;
        unset($params['id']);

        $data = $this->db->update( 
            $this->tableName,
            $params,
            ['id' => $this->id],
            $this->typeField,
            ['%d']
        );
    }
}
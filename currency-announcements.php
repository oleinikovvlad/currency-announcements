<?php
/**
 * @package currency_announcements
 * @version 1.9.2.0
 */
/*
Plugin Name: currency announcements
Plugin URI: http://wordpress.org/plugins/
Description: Плагин для объявлений о продаже и покупке биткоинов
Author: Vladislav Oleinikov
Version: 1.9.2.0
Author URI:
*/
//use App;

use caBackend\controllers\CaMainSettingController;
use caBackend\controllers\CaSecondarySettingController;
use caCommon\models\AnnouncementsInfo;
use caCommon\models\PluginTranslite;
use caCommon\models\Settings;
use caCommon\services\CheckParameterService;

define('CA_DIR', plugin_dir_path( __FILE__ ));

include 'autoload.php';

// function activate plugin
register_activation_hook( __FILE__, 'ca_activate' );
register_deactivation_hook( __FILE__, 'ca_deactivate' );

function ca_activate(){
	include 'ca-main/ca-activate.php';
}
function ca_deactivate(){
	include 'ca-main/ca-deactivate.php';
}

include 'ca-setting/ca-basic-settings.php';
include_once 'ca-setting/ca-core-translite.php';

add_action('admin_menu', 'ca_create_menu');
add_action('template_redirect', 'shortcode_init' );
add_action('plugins_loaded', 'register_endpoints' );

function ca_create_menu() {
	add_menu_page('CA Plugin Settings', 'CA Plugin Settings', 'manage_options', 'ca-main-setting');
	add_submenu_page( 'ca-main-setting', 'CA Setting Main', 	 'Main', 	  'manage_options', 'ca-main-setting',		[new CaMainSettingController(), 'beforeAction']);	
	add_submenu_page( 'ca-main-setting', 'CA Setting Secondary', 'Secondary', 'manage_options', 'ca-secondary-setting', [new CaSecondarySettingController(), 'beforeAction']);
	add_submenu_page( 'ca-main-setting', 'CA Basic Settings', 'Basic Settings', 'manage_options', 'ca-basic-settings', 'ca_basic_settings');
	add_submenu_page( 'ca-main-setting', 'CA Core Tranlite', 'Core Tranlite', 'manage_options', 'ca-core-translite', 'ca_core_translite');
}

function shortcode_init()
{
	add_shortcode( 'cur-announc', 'poll_shortcode' );
}   

function register_endpoints(){
	//регистрация точки доступа для получения оферов
	add_action('wp_ajax_ca_request',         'ca_request' );
	add_action('wp_ajax_nopriv_ca_request',  'ca_request' );

	//регистрация точки доступа для создания заявки
	add_action('wp_ajax_ca_create_order',         'ca_create_order' );
	add_action('wp_ajax_nopriv_ca_create_order',  'ca_create_order' );
}

function ca_create_order () 
{
	try {
		$result = true;
		$data = $_POST['data'];

		if (isset($data)) {
			$emailTo = get_main_info('contact_mail');
			if (isset($data['table'])) {
				$setting = Settings::findOne(['setting_name' => 'table_email']);
				if ($setting->setting_value) {
					$setting->setting_value = json_decode($setting->setting_value, true);
				} else {
					$setting->setting_value = [];
				}
				$emailTo = $setting->setting_value[$data['table']] ?? $emailTo;
			}
			if (isset($data['transaction'])) {
				$subject = 'Сообщение на '.$data['transaction']; 
			} else {
				$result = false;
			}

			$body = isset($data['id']) ? "ID: ".$data['id']." \n ": '';
			$body .= 'ID+Countru: '. get_text_mail();

			if (
				$result && 
				isset($data['amount']) && 
				isset($data['value']) && 
				isset($data['btc'])
			) {
				$body .= "Я хочу ".$data['transaction'].": ".$data['amount']." ".$data['value']. " ".$data['btc']." BTC  \n";
			} else {
				$result = false;
			}

			if (
				$result && 
				isset($data['price'])
			) {
				$body .= "Курс: " .$data['price']." ".$data['value']."/BTC  \n" ;
			} else {
				$result = false;
			}

			$body .= "Коментарий: " . isset($data['message']) ? $data['message'] : '';	

		} else {
			$result = false;
		}
		if ($result) {
			ini_set('max_execution_time', '0');
			$result = wp_mail($emailTo, $subject, $body);
		}
		if ($result) {
			wp_send_json(['result' => 'success'], 200);
		} else {
			wp_send_json(['result' => 'error'], 400);
		}
	} catch (\Throwable $th) {
		wp_send_json(['result' => 'error', 'message'=>'Php error'], 400);
	}
	wp_die();
}

function get_main_info($key){
    global $wpdb;
    $main_info = $wpdb->get_var( "SELECT $key FROM ca_main_info" );
    return  $main_info;
}

function get_text_mail()
{
	$str = get_main_info('text_mail');
	if(preg_match('/(^\[)(\w*[-_]*)*\w*(\s*\w*[-]*\w*[=]*["]*\w*[-]*\w*["]*)*(\]$)/', $str)){
		$str = do_shortcode($str);
	} 
	return $str;
}

function ca_request()
{
	global $wpdb;

	$checkService = new CheckParameterService();

	$method =  $_POST['method'];
	$language = $checkService->languageParameter($_POST['language'] ?? 'default');
	$table = $checkService->tableNameParameter($_POST['table'] ?? 'main');
	$attributes = isset($language)
		? ['lang' => $language]
		: []; 
	$modelTranslite = PluginTranslite::findOne(['translite_id'=>$language]);

	switch ($method) {
		case 'lbc':
			include 'ca-main/ca-publication-lbc.php';
			break;
		case 'local':
		case 'obo_cash':
			include 'ca-core-request.php';
			$announcements_info = AnnouncementsInfo::findAll(['visibility' => 1, 'table_view' => $table]);
			if ($language) {
				$idLanguage = $wpdb->get_row($wpdb->prepare( "SELECT * FROM ca_language WHERE lang = %s ", $language ));
				$idLanguage = $idLanguage->id ?? null;
			}
			switch ($method){
				case 'local':				
					$code = cur_announc_all($currency_info, $announcements_info, $attributes, $modelTranslite);

					wp_send_json(['result' => 'success', 'data'=> $code], 200);

					break; #close local
				case 'obo_cash':
					$response = [
						"rates" => ['count' => 0,],
						"ads" => [
							'sell' => [	'count' => 0,],
							'buy' => ['count' => 0,]
						],
					];
					
					$lengthRates = count($currency_info); 
					if ($lengthRates > 0){
						$response['rates']['count'] = $lengthRates;
						foreach ($currency_info as $key => $value) {
							$response['rates'][$key]['id'] = $value->id;
				
							if ($value->currency_name == 'BITCOIN'){
								$response['rates'][$key]['name'] = "USD";
							} else {
								$response['rates'][$key]['name'] = $value->currency_name;
							}
							
							$response['rates'][$key]['value'] = $value->currency_value;
							$response['rates'][$key]['updated'] = $value->updated_date;
				
						}
					} else {
						$response['rates']['error'] = 'No values available.';
					}
				
					$lengthAds = count($announcements_info); 
					if ($lengthAds > 0){
						foreach ($announcements_info as $value) {
							$comment = null;
							if ($idLanguage) {
								$resultSql = $wpdb->get_row( "SELECT comment FROM ca_announcements_comment WHERE id_announc = $value->id AND id_langth = $idLanguage" );
								$comment = $resultSql->comment ?? null;
							}

							switch ($value->activity) {
								case 0:
									// buy
									$index = $response['ads']['buy']['count'];
									$response['ads']['buy'][$index]["id"] 				= $value->id;
									$response['ads']['buy'][$index]["currency_name"] 	= $value->currency_name;
									$response['ads']['buy'][$index]["extra_charge"] 	= $value->extra_charge;
									$response['ads']['buy'][$index]["restriction_min"] 	= $value->restriction_min;
									$response['ads']['buy'][$index]["restriction_max"] 	= $value->restriction_max;
									$response['ads']['buy'][$index]["comment"] 			= $comment ?? $value->currency_value;
				
									$response['ads']['buy']['count'] = $index+1;
									break;
								case 1:
									//sell
									$index = $response['ads']['sell']['count'];
									$response['ads']['sell'][$index]["id"] 				= $value->id;
									$response['ads']['sell'][$index]["currency_name"] 	= $value->currency_name;
									$response['ads']['sell'][$index]["extra_charge"] 	= $value->extra_charge;
									$response['ads']['sell'][$index]["restriction_min"] = $value->restriction_min;
									$response['ads']['sell'][$index]["restriction_max"] = $value->restriction_max;
									$response['ads']['sell'][$index]["comment"] 		= $comment ?? $value->currency_value;
				
									$response['ads']['sell']['count'] = $index+1;
									break;
							}
						}
					} else {
						$response['ads']['error'] = 'No values available.';
					}

					wp_send_json(['result' => 'success', 'data'=>$response], 200);
					break; #close obo_cash
			}
			break; # close local or obo_cash
		default:
			wp_send_json(['result' => 'error'], 404);
			break;
	}
	wp_die();
}

function get_the_user_ip()
{   $ip = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //check ip from share internet
        $ip .= '(1)' . $_SERVER['HTTP_CLIENT_IP'];
    } 
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //to check ip is pass from proxy
        $ip .= ' (2)' . $_SERVER['HTTP_X_FORWARDED_FOR'];
    } 
        $ip .= ' (3)'. $_SERVER['REMOTE_ADDR'];
    return $ip;
}
	add_action('wp_enqueue_scripts','ava_test_init');
function ava_test_init() {
	wp_enqueue_script( 'ca-jqery-js', plugins_url( '/js/jquery-3.4.1.min.js', __FILE__ ));
	wp_enqueue_script( 'ca-jq-migrate-js', plugins_url( '/js/jquery-migrate-3.1.0.min.js', __FILE__ ));
	//wp_enqueue_script( 'ca-main-js', plugins_url( '/js/pop-message.js', __FILE__ ),array(), false, true);
}
## шоткод вывода объявлений
function poll_shortcode($atts){
	wp_enqueue_style( "ca-main", plugins_url('currency-announcements/css/ca-user-stylle.css') );
	echo '<script type="text/javascript" src="'.plugins_url("currency-announcements/js/pop-message.js").'" defer></script>';

	$atts = shortcode_atts( array(
		'lang'  => 'default',
		'id' 	=> null,
		'table' => 'main',
	), $atts, 'cur-announc' );
	
	global $wpdb;
	$use_localbitcoins = $wpdb->get_results( "SELECT use_localbitcoins FROM ca_main_info WHERE id = 1" );
	$use_localbitcoins = 0 + $use_localbitcoins[0]->use_localbitcoins;
	$ads_show = get_main_info('ads_show');

	$modelTranslite = PluginTranslite::findOne(['translite_id'=>$atts['lang']]);

	if($use_localbitcoins == 1){
		include_once 'ca-users/ca-lb.php';
		return creat_lb_announc($ads_show);


	} else {
		include_once 'ca-core-request.php';
		$announcements_info = AnnouncementsInfo::findAll(['visibility' => 1, 'table_view' => $atts['table']]);
		
		$html = construnct_user_table($currency_info, $announcements_info, $ads_show, $atts, $modelTranslite);
		return $html;
	}
	
}
function construnct_user_table($currency_info, $announcements_info, $ads_show, $attributes, $modelTranslite){
	$code = cur_announc_all($currency_info, $announcements_info, $attributes, $modelTranslite);
	
	echo 
	'<script>
	const langData = {
		text_price : "'.trim($modelTranslite->text_price).'",
        text_traide_limits : "'.trim($modelTranslite->text_traide_limits).'",
        text_user_comment : "'.trim($modelTranslite->text_user_comment).'",
        text_error_sell : "'.trim($modelTranslite->text_error_sell).'",
        text_error_buy : "'.trim($modelTranslite->text_error_buy).'",
		text_sell_question : "'.trim($modelTranslite->text_sell_question).'",
        text_buy_question : "'.trim($modelTranslite->text_buy_question).'",
        text_success_sell : "'.trim($modelTranslite->text_success_sell).'",
        text_success_buy : "'.trim($modelTranslite->text_success_buy).'",
        text_error_limit : "'.trim($modelTranslite->text_error_limit).'",
		text_instruction : '.json_encode(explode("\n", $modelTranslite->text_instruction)).',
	};
	</script>';


	if($ads_show == 'all'){
		$html_buy[0] = '
		<div class="column">
			<div class="contain" id="contain_buy">
				<div class="loading">
					<div class="spinner"></div>
				</div>'.$code[0].'</div></div>'
		;
		$html_sell[0] = '
		<div class="column">
			<div class="contain" id="contain_sell">
				<div class="loading" id="loading">
					<div class="spinner"></div>
				</div>'.$code[1].'</div></div>'
		;
	}
	if($ads_show == 'sell'){
		$html_buy[0] = '
		<div class="column">
			<div class="contain" id="contain_buy">
				<div class="loading">
					<div class="spinner"></div>
				</div>'.$code[0].'</div></div>'
		;
	}
	if($ads_show == 'buy'){
		$html_sell[0] = '
		<div class="column">
			<div class="contain" id="contain_sell">
				<div class="loading" id="loading">
					<div class="spinner"></div>
				</div>'.$code[1].'</div></div>'
		;
	}
	
	$html_code = isset($attributes['lang'])
		? '<div id="language" value="'.$attributes['lang'].'"></div>'
		: '';
	$html_code .= '<div id="ca_table" value="'.$attributes['table'].'"></div>';
	$html_code .= '
	<div id="id_user" value="'. get_the_user_ip().'"></div>
	<input type="hidden" id="type_ads" name="type_ads" value="local">
	<div class="popup" id="popup1">
    	<div class="popup-content">
        	<div class="popup_form">
            	<div class="popup_error">'.$modelTranslite->text_error_critical.'</div>
        	</div>
        	<div class="popup_button">
        	    <button class="button" onclick="javascript:sendAjaxForm()">'.$modelTranslite->button_send.'</button>
        	    <button class="button" onclick="javascript:PopUpHideOne()">'.$modelTranslite->button_cancel.'</button>
        	</div>
    	</div>
	</div>
	<div class="popup" id="popup2">
    	<div class="popup-content">
    	    <div class="popup_form">
    	        <div class="popup_error">'.$modelTranslite->text_error_critical.'</div>
    	    </div>
    	    <div class="popup_button">
    	        <button class="button" onclick="javascript:PopUpHideTwo()">'.$modelTranslite->button_cancel.'</button>
    	    </div>
    	</div>
	</div>
	<div class="table">';

	//на покупку
	if (
		$ads_show == 'sell'
		|| $ads_show == 'all'
	){
		$html_code .= '<div class="transaction" id="transaction_buy" value="Sell:">'.$modelTranslite->column_sell.'</div>';
	}
	//на продажу 
	if (
		$ads_show == 'buy'
		|| $ads_show == 'all'	
	){
		$html_code .= '<div class="transaction" id="transaction_sell" value="Buy:">'.$modelTranslite->column_buy.'</div>';
	} 

	$html_code .= '</div>';
	$html_code .= '<div class="table_lbc">';

	//на покупку
	if (
		$ads_show == 'all' ||
		$ads_show == 'sell'
	) {
		$html_code .= $html_buy[0];
	}
	//на продажу 
	if (
		$ads_show == 'all' ||
		$ads_show == 'buy'
	) {
		$html_code .= $html_sell[0];
	}
	$html_code .= '</div>';

	return $html_code;

}

function cur_announc_all($currency_info, $announcements_info, $attributes, $modelTranslite){	
	$html_buy = [0=>''];
	$html_sell = [0=>''];
	
	foreach ($announcements_info as $key_announ => $value) {
		
		$currency_name = $value->currency_name;
		$activity = $value->activity;

		if ($currency_name == 'USD'){
			$html_code = construc_anouncement_user($activity, $key_announ, $value, $currency_info[0]->currency_value, 1, $attributes, $modelTranslite);
			if ($activity == '0'){
				$html_buy[0] .= $html_code;
			}else {
				$html_sell[0] .= $html_code;
			}
		} else {
			foreach ($currency_info as $key_currency => $object) {
				if($currency_name == $object->currency_name){
					$html_code = construc_anouncement_user($activity, $key_announ, $value, $object->currency_value, $currency_info[0]->currency_value, $attributes, $modelTranslite);
					if ($activity == '0'){
						$html_buy[0] .= $html_code;
					}else {
						$html_sell[0] .= $html_code;
					}
				}
			}
		}
		
	}
	$code = [
		0 => $html_buy[0],
		1 => $html_sell[0]
	];
	
	return $code;

}

function construc_anouncement_user($activity, $index, $announcement, $currency_value, $btc_price = 1, $attributes, $modelTranslite){
	$activity++;
	if (isset($attributes['lang'])) {
		global $wpdb;
		$language = $attributes['lang'];
		$idLanguage = $wpdb->get_row($wpdb->prepare( "SELECT * FROM ca_language WHERE lang = %s ", $language ));
		$idLanguage = $idLanguage->id ?? null;
		$resultSql = isset($idLanguage)
			? $wpdb->get_row( "SELECT comment FROM ca_announcements_comment WHERE id_announc = $announcement->id AND id_langth = $idLanguage" )
			: null;
		$comment = $resultSql->comment ?? null;
	}
	
	$currency_name = $announcement->currency_name;
	$price = $currency_value * $btc_price;
	$extra_charge = $announcement->extra_charge;

	if ($extra_charge != '0'){
		$price = $price +($price * $extra_charge)/100;
	}
	$price = round($price, 2);

	$res_min = $announcement->restriction_min;
	$res_max = $announcement->restriction_max;
	if (!isset($comment)) {
		$comment = $announcement->currency_value;
	}
	$payment = $announcement->payment;
	if ($payment == 'bank-transfer') {
		$payment = $modelTranslite->bank_transfer;
	} elseif ($payment == 'cash-deposit') {
		$payment = $modelTranslite->cash_deposit;
	}
	$elementId = $activity.'_'.$index;

	$html_code = '<div id="'.$elementId.'" class="tile" data-price="'.$price.'" data-currency_name="'.$currency_name.'" data-limit_min="'.$res_min.'" data-limit_max="'.$res_max.'">';
	$html_code .= '<div id="price_'.$elementId.'" data-price="'.$price.'" data-currency_name="'.$currency_name.'">'.$modelTranslite->text_price.' '.$price.' '.$currency_name.'/BTC</div>';
	$html_code .= '<div>'.$modelTranslite->text_trade_limits.' '.$res_min.' - '.$res_max.' '.$currency_name.' </div>';
	$html_code .= '<div>'.$modelTranslite->text_type_payment.' '.$payment.'</div>';
	

	if(!empty($comment)){
		$comment = preg_replace('/(\n)/', '<br>', $comment);
		$html_code .= '<div style="text-align:justify;">'.$modelTranslite->text_comment.' '.$comment.'</div>';
	}
	if($activity == 1) {
		$html_code .= '<div class="transaction">'.$modelTranslite->button_sell.'</div>';
	}
	if($activity == 2) {
		$html_code .= '<div class="transaction">'.$modelTranslite->button_buy.'</div>';
	}

	
	$html_code .= '</div>';
	return $html_code;

}

<?php 
    /**
    *    обработчик, получить/обновить значения валют
    **/
    global $wpdb;

    $currency_info = $wpdb->get_results( "SELECT * FROM ca_currency_info" );
    
    $date_updated = $currency_info['0']->updated_date;
    
    if (check_fiat($date_updated, 'fiat' )){
        include dirname(__FILE__).'/ca-main/ca-apirequest.php'; 
        
        $result_fiat =   update_bd_fiat();
        $date_updated = $currency_info['1']->updated_date;
        if (check_fiat($date_updated, 'currency' )){
            $result_currency = update_bd_currency();
        } 
        $currency_info = $wpdb->get_results( "SELECT * FROM ca_currency_info" );
    } 

    function check_fiat($date, $method){
        
        $date_now = strtotime(date('c', time()));
        $date_updated = strtotime($date);

        switch ($method) {
            case 'fiat':
                if( ($date_now-$date_updated)/60 > 2) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'currency':
                if( ($date_now-$date_updated)/60 > 31) {
                    return true;
                } else {
                    return false;
                }
                break;
           
        }

    }
?>
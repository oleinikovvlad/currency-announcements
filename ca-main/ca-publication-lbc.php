<?php

use caCommon\models\PluginTranslite;

include('ca-api-localbitcoins.php');
$language = $language ?? 'default';
$modelTranslite = PluginTranslite::findOne(['translite_id'=>$language]);

// Your default Api Public Key
$API_AUTH_KEY = get_main_info('key_localbitcoins');
// Your default API Secret Key
$API_AUTH_SECRET = get_main_info('secret_localbitcoins');

// Put them to true after tests.
define('SSL_VERIFYPEER', true);
define('SSL_VERIFYHOST', true);

$Lbc_Advertisements = new LocalBitcoins_Advertisements_API($API_AUTH_KEY, $API_AUTH_SECRET);

$optional = array(
	'visible'		=>	'1',
	'trade_type'	=>	'ONLINE_SELL',
);
$res_sell = json_decode(json_encode($Lbc_Advertisements->Ads($optional)), true);

$optional = array(
	'visible'		=>	'1',
	'trade_type'	=>	'ONLINE_BUY',
);
$res_buy = json_decode(json_encode($Lbc_Advertisements->Ads($optional)), true);

/******************
 * The number of all ads
 * ONLINE_SELL, ONLINE_BUY
 ********************/

$sell_max = $res_sell['data']['ad_count'] - 1;
$sell = 0;

$buy_max = $res_buy['data']['ad_count'] - 1;
$buy = 0;

/******************
 * price
 * $price = $res_buy/$res_sell['data']['ad_list'][$buy/$sell]['data']['temp_price'];
 ********************/

/*******************
 * currency
 * $curency = $res_buy/$res_sell['data']['ad_list'][$buy/$sell]['data']['currency'];
 ********************/

/******************
 * max amount
 * $max_amount = $res_buy/$res_sell['data']['ad_list'][$buy/$sell]['data']['max_amount_available'];
 ********************/

/******************
 * min amount
 * $min_amount = $res_buy/$res_sell['data']['ad_list'][$buy/$sell]['data']['min_amount'];
 ********************/

//if (is_null($buy_max)) {
	$html_code =  '<!-- Start contain buy -->';
	$html_code .= 	'<div class="loading"><div class="spinner"></div></div>';
	for ($buy; $buy <= $buy_max; $buy++) {

		$price = $res_buy['data']['ad_list'][$buy]['data']['temp_price'];
		$curency = $res_buy['data']['ad_list'][$buy]['data']['currency'];
		$max_amount = $res_buy['data']['ad_list'][$buy]['data']['max_amount_available'];
		$min_amount = $res_buy['data']['ad_list'][$buy]['data']['min_amount'];
		
		$html_code .=  	'<div id="1_' . $buy . '" class="tile" data-price="'.$price.'" data-currency_name="'.$curency.'" data-limit_min="'.$min_amount.'" data-limit_max="'.$max_amount.'">';
		$html_code .=       '<div id="price_1_'.$buy.'" data-price="'.$price.'" data-currency_name="'.$curency.'">'.$modelTranslite->text_price.' '. $price . ' ' . $curency . '/BTC</div>';
		$html_code .=       '<div>'.$modelTranslite->text_trade_limits.' '. $min_amount . ' - ' . $max_amount . ' ' . $curency . ' </div>';
		$html_code .= 		'<div class="transaction">'.$modelTranslite->button_sell.'</div>';
		$html_code .=  	'</div>';
	}
	$html_code .=  	'<!-- End contain buy -->';
// } else {
//  	$html_code = 	'<div class="loading" id="loading"><div class="spinner"></div></div>';
//  	$html_code .= '<div>Нет доступных операций</div>';
// }

$answer['buy'] = $html_code;

$html_code =  '<!-- Start contain sell -->';
$html_code .= 	'<div class="loading"><div class="spinner"></div></div>';
// if (is_null($sell_max)) {
	for ($sell; $sell <= $sell_max; $sell++) {

		$price = $res_sell['data']['ad_list'][$sell]['data']['temp_price'];
		$curency = $res_sell['data']['ad_list'][$sell]['data']['currency'];
		$max_amount = $res_sell['data']['ad_list'][$sell]['data']['max_amount_available'];
		$min_amount = $res_sell['data']['ad_list'][$sell]['data']['min_amount'];

		$html_code .=  	'<div id="2_' . $sell . '" class="tile" data-price="'.$price.'" data-currency_name="'.$curency.'" data-limit_min="'.$min_amount.'" data-limit_max="'.$max_amount.'">';
		$html_code .=       '<div id="price_2_'.$sell.'" data-price="'.$price.'" data-currency_name="'.$curency.'">'.$modelTranslite->text_price.' '. $price . ' ' . $curency . '/BTC</div>';
		$html_code .=       '<div>'.$modelTranslite->text_trade_limits.' '. $min_amount . ' - ' . $max_amount . ' ' . $curency . ' </div>';
		$html_code .= 		'<div class="transaction">'.$modelTranslite->button_buy.'</div>';
		$html_code .=  	'</div>';
	}
	$html_code .=  	'<!-- End contain sell -->';
// } else {
//  	$html_code = 	'<div class="loading" id="loading"><div class="spinner"></div></div>';
//  	$html_code .= 	'<div>Нет доступных операций</div>';
// }
$answer['sell'] = $html_code;


$json = json_encode($answer);

echo $json;
wp_die();

?>
<?php 
  /**
  * Requires curl enabled in php.ini
  **/

function api_coinmarketcap(){
    $url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest';
    $parameters = [
        'symbol' => 'BTC',
        'convert' => 'USD'
    ];
    global $wpdb;
    $key_coinmarketcap = $wpdb->get_var( "SELECT key_coinmarketcap FROM ca_main_info" );

    $headers = [
        'Accepts: application/json',
        'X-CMC_PRO_API_KEY: '.$key_coinmarketcap
    ]; 
    $qs = http_build_query($parameters); // query string encode the parameters 
    $request = "{$url}?{$qs}"; // create the request URL
    
    
    $curl = curl_init(); // Get cURL resource
    // Set cURL options
    curl_setopt_array($curl, array(
        CURLOPT_URL => $request,            // set the request URL
        CURLOPT_HTTPHEADER => $headers,     // set the headers 
        CURLOPT_RETURNTRANSFER => 1,         // ask for raw response instead of bool
    ));
    
    $response = curl_exec($curl); // Send the request, save the response
    //print_r(json_decode($response)); // print json decoded response
    curl_close($curl); // Close request

    $response = json_decode($response);
    $error = $response->status->error_message;
    if ($error) {
        return $error;
    } else {
        return $response;
    }
}

function api_openexchangerates(){
    $url = 'https://openexchangerates.org/api/latest.json';

    global $wpdb;
    $key_openexchangerates = $wpdb->get_var( "SELECT key_openexchangerates FROM ca_main_info" );
    
    $parameters = [
        'app_id' => $key_openexchangerates,
        'symbols' => 'AUD,THB,IDR,RUB,MXN,MYR,EUR,KHR'
    ];

    $qs = http_build_query($parameters); // query string encode the parameters 
    $request = "{$url}?{$qs}"; // create the request URL

    $curl = curl_init(); // Get cURL resource

    curl_setopt_array($curl, array(
        CURLOPT_URL => $request,            // set the request URL
        CURLOPT_RETURNTRANSFER => 1,     // ask for raw response instead of bool
    ));

    $response = curl_exec($curl); // Send the request, save the response

    curl_close($curl); // Close request

    $response = json_decode($response);

    if(!empty($response->error)){
        $error = $response->description;
        return $error;
    } else {
        $response = $response -> rates;
        return $response;
    }
    
}

function update_bd_fiat(){
    $response = api_coinmarketcap();
    if(is_string($response)){
        return $response; //eror
    } else {

        $price = $response->data->BTC->quote->USD->price;
        $data = date('c', time());
        if ($price) {
        global $wpdb;
        $request = $wpdb->update( 
            'ca_currency_info',
            array(  'currency_value' => $price,
                    'updated_date'=> $data ),
            array(  'currency_name' => 'BITCOIN' ),
            array(  '%f', '%s'),
            array( '%s' )
        );
    
        return $request; // query result 1-successful 0-no updates
        }
        return 0;
    }
}

function update_bd_currency(){
    $response = api_openexchangerates();
    if(is_string($response)){
        return $response; //error
    } else { 
        $name_currency = array("AUD", "THB", "IDR", "RUB", "MXN", "MYR", "EUR", "KHR");
        foreach ($name_currency as $key => $value) {
        $data = date('c', time());
        global $wpdb;
        if ($response->$value) {
            $request = $wpdb->update( 
            'ca_currency_info',
            array(  'currency_value' => $response->$value,
                    'updated_date'=> $data ),
            array(  'currency_name' => $value ),
            array(  '%f', '%s'),
            array( '%s' )
            );  
        } 
        }

        return $request;
    }
}

<?php
class LocalBitcoins {

	public function __construct($API_AUTH_KEY = null, $API_AUTH_SECRET = null) {
        $this->API_AUTH_KEY 	= $API_AUTH_KEY;
        $this->API_AUTH_SECRET 	= $API_AUTH_SECRET;
    }

	public function Query($url,$get=array()) {

		if(!defined('SSL_VERIFYPEER'))		define('SSL_VERIFYPEER',true);
		if(!defined('SSL_VERIFYHOST'))		define('SSL_VERIFYHOST',true);

		// Method
		$api_get 	= array('/api/ads/','/api/ad-get/{ad_id}/','/api/ad-get/','/api/payment_methods/','/api/payment_methods/{countrycode}/','/api/countrycodes/','/api/currencies/','/api/places/','/api/contact_messages/{contact_id}/','/api/contact_info/{contact_id}/','/api/contact_info/','/api/account_info/{username}','/api/dashboard/','/api/dashboard/released/','/api/dashboard/canceled/','/api/dashboard/closed/','/api/myself/','/api/notifications/','/api/real_name_verifiers/{username}/','/api/recent_messages/','/api/wallet/','/api/wallet-balance/','/api/wallet-addr/','/api/merchant/invoices/','/api/merchant/invoice/{invoice_id}/');

		// Init curl
		static $ch = null; $ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; LocalBitcoins API PHP client; '.php_uname('s').'; PHP/'.phpversion().')');

		if(SSL_VERIFYPEER!==true)
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if(SSL_VERIFYHOST!==true)
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		// Build NONCE
		$mt = explode(' ', microtime());
		$API_AUTH_NONCE = $mt[1].substr($mt[0], 2, 6);

		$datas = '';
		if (in_array($url,$api_get)) {
			if (!empty($get)) {
				$datas = http_build_query($get,'','&');
			}
			curl_setopt($ch, CURLOPT_HTTPGET, true);
		}
		else
		{
			if (!empty($get))
				$datas = http_build_query($get,'','&');
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			$is_public = true;
		}


		// Add Auth
		$API_AUTH_SIGNATURE = strtoupper(hash_hmac('sha256', $API_AUTH_NONCE.($this->API_AUTH_KEY).$url.$datas, ($this->API_AUTH_SECRET)));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Apiauth-Key:'.($this->API_AUTH_KEY), 'Apiauth-Nonce:'.$API_AUTH_NONCE, 'Apiauth-Signature:'.$API_AUTH_SIGNATURE));

		// Add Get params
		$url .= '?'.$datas;

		// Let's go!
		curl_setopt($ch, CURLOPT_URL, 'https://localbitcoins.net'.$url);
		curl_setopt($ch, CURLOPT_TIMEOUT,100); //set timeout to 100 seconds
		$res = curl_exec($ch);

		// website/api error ?
		if ($res === false)
			throw new Exception('Could not get reply: '.curl_error($ch));

		// return result
		return json_decode($res);
		
		curl_close($ch); //close connection
	}
}
class LocalBitcoins_Advertisements_API extends LocalBitcoins {

	public function __construct($API_AUTH_KEY=null,$API_AUTH_SECRET=null) {
		$this->API_AUTH_KEY 	= $API_AUTH_KEY;
        $this->API_AUTH_SECRET 	= $API_AUTH_SECRET;
	}

	public function Ads($optional='') {
		if (!empty($optional))
			$res = $this->Query('/api/ads/',$optional);
		else
			$res = $this->Query('/api/ads/');
		return $res;
	}

}

<?php 

function update_ca_currency(){
    $name_currency = array("BITCOIN", "AUD", "THB", "IDR", "RUB", "MXN", "MYR", "EUR", "KHR");
    $key_currency = array("1", "2", "3", "4", "5", "6", "7", "8", "9");

    foreach ($name_currency as $i => $value) { 
        $sql = "INSERT INTO ca_currency_info (currency_name, currency_id) 
                VALUES ('{$name_currency[$i]}','{$key_currency[$i]}')"; 
        dbDelta($sql);
    }

}
function update_ca_main(){
    $sql = "INSERT INTO ca_main_info (use_localbitcoins) VALUES (1)";
    dbDelta($sql);

}
function create_table() {
	global $wpdb;
	include ABSPATH . 'wp-admin/includes/upgrade.php';
	$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";

	$table_name = 'ca_main_info';
    $sql = "CREATE TABLE {$table_name} (
        id  bigint(20) unsigned NOT NULL auto_increment,
        text_mail varchar(255) NOT NULL default '',
        contact_mail varchar(255) NOT NULL default '',
        key_coinmarketcap varchar(255) NOT NULL default '',
        key_openexchangerates varchar(255) NOT NULL default '',
        key_localbitcoins varchar(255) NOT NULL default '',
        secret_localbitcoins varchar(255) NOT NULL default '',
        use_localbitcoins bit NOT NULL default 0,
        ads_show varchar(4) NOT NULL default 'all',
        PRIMARY KEY  (id),
        KEY use_localbitcoins (use_localbitcoins)
        )
        {$charset_collate};";

    $result = dbDelta($sql);
    if (isset($result['ca_main_info']) && $result['ca_main_info'] == 'Created table ca_main_info') {
        update_ca_main();
    }
    
	$table_name = 'ca_currency_info';
    $sql = "CREATE TABLE {$table_name} (
		id             BIGINT(20) unsigned NOT NULL auto_increment,
        currency_name  VARCHAR(255) NOT NULL default ''                     COMMENT 'Название валюты',
		currency_id    BIGINT(20)   NOT NULL default 0                      COMMENT 'ID валюты для запроса',
        currency_value FLOAT        NOT NULL default 0                      COMMENT 'курс к доллару',
        updated_date   VARCHAR(255) NOT NULL default '2001-01-01 01:01:01'  COMMENT 'дата обновления',
		PRIMARY KEY  (id),
		KEY currency_id (currency_id)
	    )
	    {$charset_collate};";

    dbDelta($sql);
    update_ca_currency();
    
	$table_name = 'ca_announcements_info';
    $sql = "CREATE TABLE {$table_name} (
		id                 BIGINT(20) unsigned NOT NULL auto_increment,
        visibility         bit NOT NULL default 0                           COMMENT 'Видимость объявления',
        activity           bit NOT NULL default 0                           COMMENT 'Тип операции',
        currency_name      VARCHAR(255) NOT NULL default ''                 COMMENT 'Название валюты',
        payment            VARCHAR(255) NOT NULL default ''                 COMMENT 'Вид оплаты',
		extra_charge       FLOAT   NOT NULL default 0                       COMMENT 'Наценка',
        restriction_min    BIGINT(20)   NOT NULL default 0                  COMMENT 'Ограничение по сделке МИНИМУМ',
        restriction_max    BIGINT(20)   NOT NULL default 0                  COMMENT 'Ограничение по сделке МАКСИМУМ',
        currency_value     VARCHAR(1000) NOT NULL default ''                COMMENT 'Коментарий',
        table_view         VARCHAR(255) NOT NULL default 'main'             COMMENT 'Принадлежность к таблице',
        PRIMARY KEY  (id),
		KEY visibility (visibility),
        KEY table_view (table_view)
	    )
	    {$charset_collate};";
    dbDelta($sql);

    /**
     * таблица доступных языков перевода
     */
    $table_name = 'ca_language';
    $sql = "CREATE TABLE {$table_name} (
        id      BIGINT(20) unsigned NOT NULL auto_increment,
        lang    VARCHAR(32) NOT NULL,
        PRIMARY KEY (id),
        KEY lang(lang)
    ){$charset_collate};";
    dbDelta($sql);

    /**
     * таблица переводов описания
     */
    $table_name = 'ca_announcements_comment';
    $sql = "CREATE TABLE {$table_name} (
        id             BIGINT(20) unsigned NOT NULL auto_increment,
        id_announc     BIGINT(20)       NOT NULL default 0                           COMMENT 'id объявления',
        id_langth      BIGINT(20)       NOT NULL default 0                           COMMENT 'id языка перевода',
        comment        VARCHAR(1000)    NOT NULL default ''                          COMMENT 'Коментарий',
		PRIMARY KEY  (id),
		KEY  id_langth(id_langth)
	    )
	    {$charset_collate};";
    dbDelta($sql);

    /**
     * Таблица переводов всех элементов плагина
     */
    $table_name = 'ca_plugin_translite';
    $sql = "CREATE TABLE {$table_name} (
        id  bigint(20) unsigned NOT NULL auto_increment,
        translite_id varchar(255) NOT NULL,
        column_sell varchar(255) NOT NULL default '',
        button_sell varchar(255) NOT NULL default '',
        column_buy varchar(255) NOT NULL default '',
        button_buy varchar(255) NOT NULL default '',
        text_price varchar(255) NOT NULL default '',
        text_trade_limits varchar(255) NOT NULL default '',
        text_type_payment varchar(255) NOT NULL default '',
        bank_transfer varchar(255) NOT NULL default '',
        cash_deposit varchar(255) NOT NULL default '',
        text_comment varchar(255) NOT NULL default '',
        text_sell_question varchar(255) NOT NULL default '',
        text_buy_question varchar(255) NOT NULL default '',
        text_user_comment varchar(255) NOT NULL default '',
        text_traide_limits varchar(255) NOT NULL default '',
        text_error_limit varchar(255) NOT NULL default '',
        text_error_critical varchar(255) NOT NULL default '',
        button_send varchar(255) NOT NULL default '',
        button_cancel varchar(255) NOT NULL default '',
        text_success_sell varchar(255) NOT NULL default '',
        text_success_buy varchar(255) NOT NULL default '',
        text_error_sell varchar(255) NOT NULL default '',
        text_error_buy varchar(255) NOT NULL default '',
        text_instruction varchar(255) NOT NULL default '',
        PRIMARY KEY  (id),
        KEY translite_id(translite_id)
        )
        {$charset_collate};";

    dbDelta($sql);

    /**
     * таблица для распределения оферов
     */
    $table_name = 'ca_table_view';
    $sql = "CREATE TABLE {$table_name} (
        id bigint(20) unsigned NOT NULL auto_increment,
        id_name varchar(32) not null,
        id_key varchar(5) not null,
        PRIMARY KEY (id),
        KEY id_key(id_key)
    ){$charset_collate}";
    dbDelta($sql);

    /**
     * Таблица настроек плагина
     */
    $table_name = 'ca_settings';
    $sql = "CREATE TABLE {$table_name} (
        id bigint(20) unsigned NOT NULL auto_increment,
        setting_name varchar(255) NOT NULL,
        setting_value varchar(255) NOT NULL default '',
        PRIMARY KEY (id),
        KEY setting_name(setting_name)
    ){$charset_collate}";
    dbDelta($sql);

}

create_table();
?>
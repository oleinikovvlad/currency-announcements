$(window).on('load', function () {
    type_ads = document.getElementById('type_ads').value;
    elLanguage = document.getElementById('language');
    elCaTable = document.getElementById('ca_table');
    if (elLanguage) {
        language = elLanguage.getAttribute('value')
    }
    if (elCaTable) {
        caTable = elCaTable.getAttribute('value')
    }

    switch (type_ads) {
        case 'local':
            closeLoader();
            setTimeout(request, 1000 * 60);
            break;
        case 'lbc':
            request();
            break;
    }

    $(".tile").click(createId);
});


const URL = window.location.protocol + "//" + window.location.host;
var idTransaction;
var getOffers = true;
var openPopup = false;
var min, max, price;
var sendForm = false;
var type_ads;
var language = null;
var caTable = 'main';


function request() {
    if (getOffers) {
        openLoader();
        var data = {
            action: 'ca_request',
            method: type_ads,
            table: caTable
        };
        if (language) {
            data.language = language;
        }
        $.ajax({
            url: URL + '/wp-admin/admin-ajax.php',
            type: "POST",
            data: data,
            success: function (response) {
                if (!sendForm) {
                    switch (type_ads) {
                        case 'local':
                            $('#contain_sell').html(response.data[1]);
                            $('#contain_buy').html(response.data[0]);
                            break;
                        case 'lbc':
                            $('#contain_sell').html(response.data['sell']);
                            $('#contain_buy').html(response.data['buy']);
                            break;
                    }
                }
    
                closeLoader();
                if (openPopup) { modal() };
                $(".tile").click(createId);
                setTimeout(request, 1000 * 60);
            },
            error: function (response) {
                $('#contain_sell').html('<div class="tile">'+langData.text_error_sell+'</div>');
                $('#contain_buy').html('<div class="tile">'+langData.text_error_buy+'</div>');
                setTimeout(request, 1000 * 60);
                closeLoader();
                modal();
            }
        });
    }
}

function openLoader() {
    var elems = document.querySelectorAll(".loading");
    for (var i = 0; i < elems.length; i++) {
        elems[i].style.display = "flex";
    }
}
function closeLoader() {
    var elems = document.querySelectorAll(".loading");
    for (var i = 0; i < elems.length; i++) {
        elems[i].style.display = "none";
    }
}

$(".transaction").click(function () {
    var id = this.id;
    if (id == 'transaction_buy') {
        if (document.getElementById("contain_buy").style.display == 'block') {
            document.getElementById("contain_buy").style.display = 'none';

        } else {
            document.getElementById("contain_buy").style.display = 'block';
            if (document.getElementById("contain_sell"))
            document.getElementById("contain_sell").style.display = 'none';
        }
    }
    if (id == 'transaction_sell') {
        if (document.getElementById("contain_sell").style.display == 'block') {
            document.getElementById("contain_sell").style.display = 'none';

        } else {
            document.getElementById("contain_sell").style.display = 'block';
            if(document.getElementById("contain_buy"))
            document.getElementById("contain_buy").style.display = 'none';
        }
    }
});

function createId() {
    idTransaction = this.id;
    modal();
}
function modal() {
    var elementData = $('#' + idTransaction).data();
    price = elementData.price;
    min = elementData.limit_min;
    max = elementData.limit_max;
    value = elementData.currency_name;

    if (!isNaN(price && min && max)) {
        if (idTransaction.includes("1_")) {
            html_code = "<span id='transaction' value='sell'>"+langData.text_sell_question+"</span>";
        }
        if (idTransaction.includes("2_")) {
            html_code = "<span id='transaction' value='buy'>"+langData.text_buy_question+"</span>";
        }
        html_code = html_code + constructForm();
        $('#popup1 .popup_form').html('<form action="">' + html_code + '</form>');
    }

    PopUpShowOne();

    document.getElementById('amount_input').oninput = function () {
        var input = +  document.getElementById('amount_input').value;
        if (input >= min) {
            if (input <= max) {
                addClassTrue();
            } else {
                addClassError();
            }
        }
        else {
            addClassError();
        }
        input = input / price;
        input = input.toFixed(6);
        document.getElementById('amount_BTC').value = input;

    }
    document.getElementById('amount_BTC').oninput = function () {
        var input = document.getElementById('amount_BTC').value;
        if (input >= (min / price)) {
            if (input <= (max / price)) {
                addClassTrue();
            } else {
                addClassError();
            }
        }
        else {
            addClassError();
        }
        input = input * price;
        input = input.toFixed(2);
        document.getElementById('amount_input').value = input;
    }
}

function addClassTrue() {
    $("#amount_input").removeClass('error_input').html();
    $("#amount_input").addClass('true_input').html();
    $("#amount_BTC").removeClass('error_input').html();
    $("#amount_BTC").addClass('true_input').html();
};
function addClassError() {
    $("#amount_input").removeClass('true_input').html();
    $("#amount_input").addClass('error_input').html();
    $("#amount_BTC").removeClass('true_input').html();
    $("#amount_BTC").addClass('error_input').html();
};
function PopUpHideOne() {
    openPopup = false;
    $("#popup1").hide();
};
function PopUpHideTwo() {
    $("#popup2").hide();
};

function PopUpShowOne() {
    openPopup = true;
    $("#popup1").show();

};
function PopUpShowTwo() {
    $("#popup2").show();
};


function constructForm() {
    let html_code;
    html_code = "<span id='price' value='" + price + "'>"+langData.text_price +" " + price + " " + value + "/BTC</span> ";
    html_code = html_code + "<div id='calculator'>";
    textTradeLimits = langData.text_traide_limits.replace(/{min}/g, min).replace(/{max}/g, max);
    html_code = html_code + "<span id='amount' required>" + value + " <input type='text' id='amount_input' placeholder='0.00' title='"+textTradeLimits+"'></span>";
    html_code = html_code + "<span id='BTC' required>BTC <input type='text' id='amount_BTC' placeholder='0.000000'></span>";
    html_code = html_code + "</div>";
    html_code = html_code + "<textarea id='textarea' rows='5' cols='34' name='text' placeholder='"+langData.text_user_comment+"'></textarea>";
    return html_code;
};


function constructAnswer(inputBTC, inputAmount) {
    let html_code;
    let transaction = document.getElementById('transaction').getAttribute('value');
    if (transaction.includes("sell")) {
        html_code = "<p>"+langData.text_success_sell+"</p>";
    }
    if (transaction.includes("buy")) {
        html_code = "<p>"+langData.text_success_buy+"</p>";
    }
    html_code = html_code + "<p>"+langData.text_price+" " + price + " " + value + "/BTC</p>";
    html_code = html_code + "<p>" + inputAmount + " " + value + "</p> ";
    html_code = html_code + "<p>" + inputBTC + " BTC</p> <hr>";
    langData.text_instruction.forEach((value) => {
        html_code = html_code + '<p>' + value + '</p>';
    });
    return html_code;
}

function sendAjaxForm() {
    
    if (!sendForm) {
        sendForm = true;
        var inputBTC = + document.getElementById('amount_BTC').value;
        var inputAmount = + document.getElementById('amount_input').value;
        var data = {
            action: 'ca_create_order',
            data : {
                transaction: document.getElementById('transaction').getAttribute('value'),
                value: value,    
                amount: inputAmount,
                btc: inputBTC,
                price: price,
                message: document.getElementById('textarea').value,
                id: document.getElementById('id_user').getAttribute('value'),
                table: caTable,
            }
        };
        
        if ((inputAmount >= min) && (inputAmount <= max)) {
            $.ajax({
                url: URL + '/wp-admin/admin-ajax.php',
                type: "POST",
                data: data,
                success: function (response) {
                    PopUpHideOne();
                    $('#contain_sell').html(constructAnswer(inputBTC, inputAmount));
                    $('#contain_buy').html(constructAnswer(inputBTC, inputAmount));
                    getOffers = false;
                },
                error: function (response) {
                    PopUpHideOne();
                    // $('.popup_form').html('<div class="error_input>' + response.responseJSON.message + '</div>');
                    PopUpShowTwo();
                    sendForm = false;
                }
            });
        } else {
            sendForm = false;
            alert(langData.text_error_limit);
        }
    }
};



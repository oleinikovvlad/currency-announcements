window.onload = function() {
    document.getElementById('new-order-href').onclick = update_table;

    listen_enter();
    
};

function listen_enter(){
    var el = document.querySelectorAll('input');
    for(var i=0; i < el.length; i++){
        el[i].addEventListener('keydown', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
            }
        }, false);
    }
}

function update_table(){
    id    =  this.getAttribute('data-id');
    id++;
    
    table =  create_table_visibility (id);
    table += create_table_activity(id);
    table += create_table_currency_name(id);
    table += create_table_payment(id);
    table += create_table_extra_charge(id);
    table += create_table_tr_two(id);

    this.setAttribute('data-id', id);
    document.getElementById('new-order').insertAdjacentHTML('beforebegin', table);

    listen_enter();
}


function create_table_visibility (id){
    
    delet_id = document.getElementsByName('delet-announcements').length;
    delet_id = delet_id;

    table =     '<tr valign="top" ><td rowspan="2" name="delet-announcements" id="delet-announcements-'+id+'" width="30px" >';
    table +=    '<button name="delet" id="delet-announcements-'+id+'" value="'+id+'">X</button>';
    table +=    '<input name="delet-'+ delet_id +'" value="'+id+'" hidden/></td>';
    table +=    '<td width="180px"><select name="visibility-'+id+'" id="visibility-'+id+'">';
    table +=    '<option value="none" hidden="">Видимость объявления</option><option value="closed">Скрыть объявление</option><option value="open">Показать объявление</option>';
    table +=    '</select></td>';

    return table;
}

function create_table_activity (id){
    table =     '<td width="100px"><select name="activity-'+id+'" id="activity-'+id+'">';
    table +=    '<option value="none" hidden="">Выберите тип</option><option value="0">Купить</option><option value="1">Продать</option></select></td>';
    return table;
}

function create_table_currency_name (id){
    name_currency = ['USD','AUD','THB','IDR','RUB','MXN','MYR','EUR', 'KHR'];
    
    table ='<td width="70px"><select name="currency-name-'+id+'" id="currency-name-'+id+'"><option value="none" hidden="">Выберите валюту</option>';
        name_currency.forEach(key => (table += '<option value="'+key+'">'+key+'</option>') );
    table += '</select></td>';
    
    return table;
}

function create_table_payment (id){
    table =  '<td width="100px"><select name="payment-'+id+'" id="payment-'+id+'">';
    table += '<option value="none" hidden="">Оплата</option><option value="cash-deposit">Cash deposit</option><option value="bank-transfer">Bank transfer</option></select></td>';
    
    return table;
}


function create_table_extra_charge (id){
    table = '<td>Процент к цене <input type="number" step="0.01" name="extra-charge-'+id+'"/></td></tr>'
    
    return table;
}

function create_table_tr_two (id){
    table =  '<tr valign="top"><td colspan="4">Лимиты на операцию'
    table += '<input type="number" id="restriction-min-'+id+'" name="restriction-min-'+id+'"/>';
    table += '<input type="number" id="restriction-max-"'+id+'" name="restriction-max-'+id+'"/></td>';
    table += '<td>Коментарий  <textarea name="currency-value-'+id+'" id="currency-value-'+id+'" ></textarea></td></tr>';
                                

    return table;
}





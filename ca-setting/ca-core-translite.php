<?php 
use caCommon\models\PluginTranslite;

function ca_core_translite() {
    global $wpdb;

    $param = $_GET['translite'] ?? 'default';
    $languages = $wpdb->get_results ( "SELECT * FROM ca_language" );

    if ($param !== 'default') {
        $langIsSet = false;
        foreach ($languages as $item) {
            if ($item->lang === $param) {
                $langIsSet = true;
                break;
            }
        }

        if (!$langIsSet) {
            ?>
            <div>
                Вы используете не дейстительную ссылку, <a href="?page=ca-core-translite">обновите</a> страницу.
            </div>
            
            <?php
            return 0;
        }
    }
    $model = PluginTranslite::findOne(['translite_id'=>$param]);
    if ($model->translite_id !== $param) {
        $model->translite_id = $param;
    }

    if (isset($_POST['update']) && $_POST['update']) {
        $model->loadData($_POST);
        $model->save();
    }


    $lang = new stdClass();
    $lang->id = '0';
    $lang->lang = 'default';

    $menu = array_merge([$lang], $languages);
?>
<div>
    <table>
        <tr>
            <td>Индетификаторы перевода:</td>
        </tr>
        <tr>
        <?php foreach ($menu as $item): ?>
            <td>
            <?php if ($item->lang == $param): ?>
                <span><?=$item->lang?></span>
            <?php else: ?>
                <a href="?page=ca-core-translite&translite=<?=$item->lang?>"><?=$item->lang?></a>
            <?php endif; ?>
            </td>
        <?php endforeach; ?>
        </tr>
    </table>
</div>
<div>
    <form method="post" action="">
        <table class="form-table" border="1">
            <tr>
                <td>Индетификатор перевода:</td>
                <td>
                    "<?= $model->translite_id ?>"
                    <input type="hidden" name="translite_id" value="<?= $model->translite_id ?>"/>
                </td>
            </tr>
            <tr>
                <td>Колонка продать биткоин:</td>
                <td>
                    <input type="text" name="column_sell" value="<?=$model->column_sell ?>">
                </td>
            </tr>
            <tr>
                <td>Кнопка продать биткоин:</td>
                <td>
                    <input type="text" name="button_sell" value="<?=$model->button_sell ?>">
                </td>
            </tr>
            <tr>
                <td>Колонка купить биткоин:</td>
                <td>
                    <input type="text" name="column_buy" value="<?=$model->column_buy ?>">
                </td>
            </tr>
            <tr>
                <td>Кнопка купить биткоин:</td>
                <td>
                    <input type="text" name="button_buy" value="<?=$model->button_buy ?>">
                </td>
            </tr>
            <tr>
                <td colspan="2">Текстовка офера</td>
            </tr>
            <tr>
                <td>Текст цена:</td>
                <td>
                    <input type="text" name="text_price" value="<?=$model->text_price ?>">
                </td>
            </tr>
            <tr>
                <td>Лимит заявки:</td>
                <td>
                    <input type="text" name="text_trade_limits" value="<?=$model->text_trade_limits ?>">
                </td>
            </tr>
            <tr>
                <td>Тип оплаты:</td>
                <td>
                    <input type="text" name="text_type_payment" value="<?=$model->text_type_payment ?>">
                </td>
            </tr>
            <tr>
                <td>Вид оплаты "перевод"</td>
                <td>
                    <input type="text" name="bank_transfer" value="<?=$model->bank_transfer ?>">
                </td>
            </tr>
            <tr>
                <td>Вид оплаты "наличный"</td>
                <td>
                    <input type="text" name="cash_deposit" value="<?=$model->cash_deposit ?>">
                </td>
            </tr>
            <tr>
                <td>Коментарий предложения:</td>
                <td>
                    <input type="text" name="text_comment" value="<?=$model->text_comment ?>">
                </td>
            </tr>
            <tr>
                <td colspan="2">Текстовка оформления заявки</td>
            </tr>
            <tr>
                <td>Вопрос клиенту на продажу:</td>
                <td>
                    <input type="text" name="text_sell_question" value="<?=$model->text_sell_question ?>">
                </td>
            </tr>
            <tr>
                <td>Вопрос клиенту на покупку:</td>
                <td>
                    <input type="text" name="text_buy_question" value="<?=$model->text_buy_question ?>">
                </td>
            </tr>
            <tr>
                <td>Подсказка для коментария пользователя</td>
                <td>
                    <input type="text" name="text_user_comment" value="<?=$model->text_user_comment ?>">
                </td>
            </tr>
            <tr>
                <td>Описание лимита для пользователя (для подставки минимального значения, используйте: {min}, максимального {max})</td>
                <td>
                    <textarea rows='2' cols='50' name='text_traide_limits'><?=$model->text_traide_limits ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Ошибка если не верный лимит заявки</td>
                <td>
                    <textarea rows='2' cols='50' name='text_error_limit'><?=$model->text_error_limit ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Критическая ошибка</td>
                <td>
                    <textarea rows='2' cols='50' name='text_error_critical'><?=$model->text_error_critical ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Кнопка "отправить"</td>
                <td>
                    <input type="text" name="button_send" value="<?=$model->button_send ?>">
                </td>
            </tr>
            <tr>
                <td>Кнопка "отмена"</td>
                <td>
                    <input type="text" name="button_cancel" value="<?=$model->button_cancel ?>">
                </td>
            </tr>
            <tr>
                <td colspan="2">Обработка ответов на зявку</td>
            </tr>
            <tr>
                <td>Текст в заголовке на "продажу"</td>
                <td>
                    <input type="text" name="text_success_sell" value="<?=$model->text_success_sell ?>">
                </td>
            </tr>
            <tr>
                <td>Текст в заголовке на "покупку"</td>
                <td>
                    <input type="text" name="text_success_buy" value="<?=$model->text_success_buy ?>">
                </td>
            </tr>
            <tr>
                <td>Текст интсрукция пользователю</td>
                <td>
                    <textarea rows='5' cols='50' name='text_instruction'><?=$model->text_instruction ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Ошибка при оформления заявки на "продажу"</td>
                <td>
                    <textarea rows='5' cols='50' name='text_error_sell'><?=$model->text_error_sell ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Ошибка при оформления заявки на "покупку"</td>
                <td>
                    <textarea rows='5' cols='50' name='text_error_buy'><?=$model->text_error_buy ?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="submit">
                        <input type="hidden" name="id" value="<?= $model->id ?>"/>
                        <input type="submit" name="update" class="button-primary" value="<?php _e('Обновить') ?>"/>
                    </p>
                </td>
            </tr>
        </table>
    </form>  
</div>

<?php } ?>

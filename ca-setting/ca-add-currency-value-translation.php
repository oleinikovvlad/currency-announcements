
<div class="wrap">
    <a href="?page=ca-secondary-setting">Вернуться назад</a>
    <?php if (!$announcements) : ?>
       <p>Офер не найден</p>
    <?php elseif (!$languages) : ?>
        <a href="?page=ca-basic-settings">Необходимо завести индетификаторы для перевода</a>
    <?php else :?>
        <form method="post" action="">
            <table class="form-table">
                <?= $languageMessage ? "<tr>$languageMessage</tr>": '' ; ?>
                <tr>       
                    <th scope="row">Добавить перевод</th>
                    <td width="100px">
                        <select name="language-id">
                            <option value="none" hidden="">Выберите перевод</option>
                            <?php foreach ($languages as $lang) :?>
                                <option value="<?= $lang->id?>"><?= $lang->lang?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td> Коментарий  <textarea name="currency-value"><?=$currencyValue?></textarea>
                    <td>
                        <p class="submit">
                            <input type="submit" name="add-announcements-comment" class="button-primary" value="<?php _e('Добавить перевод') ?>"/>
                        </p>
                    </td>
                </tr>
            </table>
        </form>
        <?php if ($arrayTranslite) :?>
            <form method="post" action="">
                <table class="form-table" border="1">
                    <?php foreach ($arrayTranslite as $translite) :?>
                        <tr>
                            <td>Индетификатор объявления "<?=$translite->id_announc ?>"</td>
                            <td>Индетификатор перевода "<?=$sortLang[$translite->id_langth]->lang ?>"</td>
                            <td rowspan="2">
                                <p class="submit">
                                    <input type="submit" name="delete-announcements-comment-<?= $translite->id ?>" class="button-primary delete-translite" value="<?php _e('Удалить значение') ?>"/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>Коментарий</td>
                            <td>"<?=$translite->comment ?>"</td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </form>

            <script>
                var elements = document.getElementsByClassName('delete-translite');

                for (let item of elements) {
                    item.addEventListener(
                        "click",
                        function(e) {
                            if (confirm('Вы действительно хотите удалить?')) {
                            } else {
                                e.preventDefault();
                            }
                        }, 
                        false
                    );
                }
            </script>
        <?php endif;?>
    <?php endif;?>
</div>

<?php

use caCommon\models\PluginTranslite;

function ca_basic_settings() {
        global $wpdb;
        $post = $_POST;

        #Проверяем есть ли данные для записи нового индетификатора перевода
        if(!empty($post['update-basic-settings'])) {
            $languageName = $post['language-name'];
            $wpdb->insert(
                'ca_language',
                array('lang' => $languageName),
                array('%s')
            );
            unset($post);
        }
        $languages = $wpdb->get_results ( "SELECT * FROM ca_language" );

        #проверяем есть ли индетификатор который нужно удалить
        if ($post) {
            foreach ($languages as $index => $lang) {
                $postName = "delete-lang-item-".$lang->id;
                if(!empty($post[$postName])) {
                    $wpdb->delete(
                        'ca_language',
                        ['id'=> $lang->id],
                        ['%d']
                    );
                    $wpdb->delete(
                        'ca_announcements_comment',
                        ['id_langth'=> $lang->id]
                        ['%d']
                    );
                    $wpdb->delete(
                        PluginTranslite::tableName(),
                        ['translite_id'=> $lang->lang],
                        ['%s']
                    );
    
                    unset($languages[$index]);
                    break;
                }     
            }
        }
    
?>  
    <div class="wrap">
        <h2>Currency announcements</h2>

        <form method="post" action="">
            <table class="form-table">
                <tr>       
                    <th scope="row">Индетификатор языка</th>
                    <td><input type="text" name="language-name" value="" /></td>
                    <td>
                        <p class="submit">
                            <input type="submit" name="update-basic-settings" class="button-primary" value="<?php _e('Добавить значение') ?>" />
                        </p>
                    </td>
                </tr>
            </table>
        </form>

        <form method="post" action="">
            <table class="form-table">
            <?php foreach($languages as $lang) :?>
                <tr>       
                    <th scope="row">Индетификатор языка : </th>
                    <td><?=$lang->lang?></td>
                    <td>
                        <p class="submit">
                            <input type="submit" name="delete-lang-item-<?= $lang->id ?>" class="button-primary delete-translite" value="<?php _e('Удалить значение') ?>"/>
                        </p>
                    </td>
                </tr>
            <?php endforeach ?>
            </table>
        </form>
        <script>
            var elements = document.getElementsByClassName('delete-translite');

            for (let item of elements) {
                item.addEventListener(
                    "click",
                    function(e) {
                        if (confirm('Вы действительно хотите удалить?')) {
                        } else {
                            e.preventDefault();
                        }
                    }, 
                    false
                );
            }
        </script>
    </div>
<?php } ?>
